#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ) {
		p=head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if(tail==0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	Node *tmp = tail;
	tail = new Node(el, 0);
	tmp->next = tail;
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
    /*
        Look ahead 2 nodes, if it's a null then delete 1 node ahead.
    */
    char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
        head = tail = 0;   
    }
	while(tmp->next->next != 0)
	{
        tmp = tmp->next;
    }
    tail = tmp;
    tmp = tmp->next;
    tail->next = 0;
    delete tmp;
	return el;
}
bool List::search(char el)
{
    Node *tmp = head;
	while(tmp != 0)
	{
        if(tmp->data == el)
        {
            return true;   
        }
        tmp = tmp->next;   
    }
	return false;
}
void List::reverse()
{
    /*
        Basically reverse all of the 'next' of each node.
    */
	Node *pHead = head, *tmp = head->next, *prev = head, *prev2 = head;
	head->next = 0;
	while(tmp != 0)
	{
        prev = tmp;
        tmp = tmp->next;
        prev->next = prev2;
        prev2 = prev;
    }
    head = tail;
    tail = pHead;
}
void List::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}
