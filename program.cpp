#include <iostream>
#include "list.h"
using namespace std;

int main()
{
	//Sample Code
	List mylist;
	cout << "Add lists." << endl;
	mylist.pushToHead('k');
	mylist.pushToHead('e');
	mylist.pushToHead('n');
	mylist.pushToHead('n');
	mylist.pushToTail('n');
	cout << "List: ";
    mylist.print();
    cout << endl;
	cout << "Remove lists." << endl;
	mylist.popTail();
	mylist.popHead();
	cout << "List: ";
	mylist.print();
    cout << endl;
	cout << "Reverse." << endl;
	mylist.reverse();
	cout << "List: ";
	mylist.print();
	cout << endl;

	//TO DO! Write a program that tests your list library - the code should take characters, push them onto a list, 
	//- then reverse the list to see if it is a palindrome!
	return 0;
}
